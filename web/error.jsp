<%@ page import="java.util.TreeMap" %>
<%@ page import="java.util.HashMap" %><%--
  Created by IntelliJ IDEA.
  User: Dmitriy
  Date: 03.04.2016
  Time: 19:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isErrorPage="true" %>
<html>
<head>
    <title>Ошибка</title>
</head>
<body>
    <%
        if(exception.getMessage().equals("Деление на 0!")){
            out.print("<h1>Возникла ошибка деления на 0! Вернуться <a href=\"calculator.jsp\">назад</a>.</h1>");
        }else {
            out.print("<h1>Возникла ошибка при вводе данных! Вернуться <a href=\"calculator.jsp\">назад</a>.</h1>");
        }
    %>
</body>
</html>

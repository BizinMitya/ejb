<%@ page import="calculator.Calculator" %>
<%@ page import="calculator.CalculatorHome" %>
<%@ page import="javax.naming.InitialContext" %>
<%--
  Created by IntelliJ IDEA.
  User: Dmitriy
  Date: 03.04.2016
  Time: 19:38
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@page errorPage="error.jsp" %>
<html>
<head>
    <title>Калькулятор</title>
</head>
<body>

<%--<form action="calculator.jsp" method="POST">
    <p><b>Первое число:</b>
        <input type="text" name="operands1">
    </p>
    <p><b>Второе число:</b>
        <input type="text" name="operands2">
    </p>
    <input type="submit" name="sum" value="Сложить">
    <input type="submit" name="sub" value="Вычесть">
    <input type="submit" name="mul" value="Умножить">
    <input type="submit" name="div" value="Разделить">
    <input type="submit" name="saveResultInMemory" value="Сохранить результат в память">
    <input type="submit" name="fromMemoryToOperands1" value="Сохранить из памяти в первый операнд">
    <input type="submit" name="fromMemoryToOperands2" value="Сохранить из памяти во второй операнд">
    <input type="submit" name="resetMemory" value="Сбросить память">
</form>--%>

<%
    Calculator calculator = (Calculator) session.getAttribute("calculator");
    if (calculator == null) {
        InitialContext initialContext = new InitialContext();
        CalculatorHome home = (CalculatorHome) initialContext.lookup("ejb:EJB_ear_exploded/ejb/CalculatorBean!calculator.CalculatorHome");
        calculator = home.create();
        session.setAttribute("calculator", calculator);
    }

    if (request.getMethod().equalsIgnoreCase("GET")) {
        out.print("<form action=\"calculator.jsp\" method=\"POST\">");
        out.print("<p><b>Первое число:</b>" +
                "<input type=\"text\" name=\"operands1\">" +
                "</p>" +
                "<p><b>Второе число:</b>" +
                "<input type=\"text\" name=\"operands2\">" +
                "</p>");
        out.print("<input type=\"submit\" name=\"sum\" value=\"Сложить\">" +
                "<input type=\"submit\" name=\"sub\" value=\"Вычесть\">" +
                "<input type=\"submit\" name=\"mul\" value=\"Умножить\">" +
                "<input type=\"submit\" name=\"div\" value=\"Разделить\">" +
                "<input type=\"submit\" name=\"saveResultInMemory\" value=\"Сохранить результат в память\">" +
                "<input type=\"submit\" name=\"fromMemoryToOperands1\" value=\"Сохранить из памяти в первый операнд\">" +
                "<input type=\"submit\" name=\"fromMemoryToOperands2\" value=\"Сохранить из памяти во второй операнд\">" +
                "<input type=\"submit\" name=\"resetMemory\" value=\"Сбросить память\">");
        out.print("</form>");
    }
    if (request.getMethod().equalsIgnoreCase("POST")) {
        String operands1 = request.getParameter("operands1");
        String operands2 = request.getParameter("operands2");
        String sum = request.getParameter("sum");
        String sub = request.getParameter("sub");
        String mul = request.getParameter("mul");
        String div = request.getParameter("div");
        String saveResultInMemory = request.getParameter("saveResultInMemory");
        String fromMemoryToOperands1 = request.getParameter("fromMemoryToOperands1");
        String fromMemoryToOperands2 = request.getParameter("fromMemoryToOperands2");
        String resetMemory = request.getParameter("resetMemory");
        if (sum != null) {
            calculator.sum(Double.parseDouble(operands1), Double.parseDouble(operands2));
        }
        if (sub != null) {
            calculator.sub(Double.parseDouble(operands1), Double.parseDouble(operands2));
        }
        if (mul != null) {
            calculator.mul(Double.parseDouble(operands1), Double.parseDouble(operands2));
        }
        if (div != null) {
            if (Double.parseDouble(operands2) == 0) {
                throw new Exception("Деление на 0!");
            } else {
                calculator.div(Double.parseDouble(operands1), Double.parseDouble(operands2));
            }
        }
        if (saveResultInMemory != null) {
            calculator.saveResultInMemory();
        }
        if (fromMemoryToOperands1 != null) {
            calculator.fromMemoryToOperands1();
        }
        if (fromMemoryToOperands2 != null) {
            calculator.fromMemoryToOperands2();
        }
        if (resetMemory != null) {
            calculator.resetMemory();
        }

        out.print("<form action=\"calculator.jsp\" method=\"POST\">");
        out.print("<p><b>Первое число:</b>" +
                "<input type=\"text\" name=\"operands1\" value=\"" + calculator.getOperands1() + "\">" +
                "</p>" +
                "<p><b>Второе число:</b>" +
                "<input type=\"text\" name=\"operands2\" value=\"" + calculator.getOperands2() + "\">" +
                "</p>");
        out.print("<input type=\"submit\" name=\"sum\" value=\"Сложить\">" +
                "<input type=\"submit\" name=\"sub\" value=\"Вычесть\">" +
                "<input type=\"submit\" name=\"mul\" value=\"Умножить\">" +
                "<input type=\"submit\" name=\"div\" value=\"Разделить\">" +
                "<input type=\"submit\" name=\"saveResultInMemory\" value=\"Сохранить результат в память\">" +
                "<input type=\"submit\" name=\"fromMemoryToOperands1\" value=\"Сохранить из памяти в первый операнд\">" +
                "<input type=\"submit\" name=\"fromMemoryToOperands2\" value=\"Сохранить из памяти во второй операнд\">" +
                "<input type=\"submit\" name=\"resetMemory\" value=\"Сбросить память\">");
        out.print("</form>");
        out.print("<p><b>Результат: " + calculator.getResult() + "</b></p>");
        out.print("<p><b>Память: " + calculator.getMemory() + "</b></p>");
    }
%>
</body>
</html>

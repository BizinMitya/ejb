package calculator;

import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

/**
 * Created by Dmitriy on 12.04.2016.
 */
public class CalculatorBean implements SessionBean {
    private double operands1;
    private double operands2;
    private StringBuffer result = new StringBuffer();
    private StringBuffer memory = new StringBuffer();

    public CalculatorBean() {
    }

    public void sum(double operands1, double operands2){
        this.operands1 = operands1;
        this.operands2 = operands2;
        getResult().delete(0, getResult().length());
        getResult().append(operands1 + operands2);
    }

    public void sub(double operands1, double operands2) {
        this.operands1 = operands1;
        this.operands2 = operands2;
        getResult().delete(0, getResult().length());
        getResult().append(operands1 - operands2);
    }

    public void mul(double operands1, double operands2) {
        this.operands1 = operands1;
        this.operands2 = operands2;
        getResult().delete(0, getResult().length());
        getResult().append(operands1 * operands2);
    }

    public void div(double operands1, double operands2) {
        this.operands1 = operands1;
        this.operands2 = operands2;
        getResult().delete(0, getResult().length());
        getResult().append(operands1 / operands2);
    }

    public void saveResultInMemory() {
        getMemory().delete(0, getResult().length());
        getMemory().append(getResult());
    }

    public void fromMemoryToOperands1() {
        operands1 = Double.parseDouble(String.valueOf(getMemory()));
    }

    public void fromMemoryToOperands2() {
        operands2 = Double.parseDouble(String.valueOf(getMemory()));
    }

    public void resetMemory() {
        getMemory().delete(0, getResult().length());
    }

    public void ejbCreate() {
    }

    public StringBuffer getResult() {
        return result;
    }

    public StringBuffer getMemory() {
        return memory;
    }

    @Override
    public void setSessionContext(SessionContext sessionContext) {

    }

    @Override
    public void ejbRemove() {

    }

    @Override
    public void ejbActivate() {

    }

    @Override
    public void ejbPassivate() {

    }

    public double getOperands1() {
        return operands1;
    }

    public double getOperands2() {
        return operands2;
    }
}

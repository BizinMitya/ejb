package calculator;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;
import java.rmi.RemoteException;

/**
 * Created by Dmitriy on 12.04.2016.
 */
public interface CalculatorHome extends EJBHome {
    Calculator create() throws RemoteException, CreateException;
}

package calculator;

import javax.ejb.EJBObject;
import java.rmi.RemoteException;

/**
 * Created by Dmitriy on 12.04.2016.
 */
public interface Calculator extends EJBObject {
    void sum(double operands1, double operands2) throws RemoteException;

    void sub(double operands1, double operands2) throws RemoteException;

    void mul(double operands1, double operands2) throws RemoteException;

    void div(double operands1, double operands2) throws RemoteException;

    void saveResultInMemory() throws RemoteException;

    void fromMemoryToOperands1() throws RemoteException;

    void fromMemoryToOperands2() throws RemoteException;

    void resetMemory() throws RemoteException;

    StringBuffer getResult() throws RemoteException;

    StringBuffer getMemory() throws RemoteException;

    double getOperands1() throws RemoteException;

    double getOperands2() throws RemoteException;
}
